# CURSO EAyO: Introducción a la Programación en Python

Clases Virtuales: Martes de 17 a 20hs <br> 

**Temario**

[[_TOC_]]

## Clase 1: Algoritmos
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-python/-/blob/main/Recursos/2021EAyO_Python_Cls_1.pdf) <br>
- [Video - Clase grabada](https://www.youtube.com/watch?v=51Jvv16GeHE) <br>

## Clase 2: Ejercicios
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-python/-/blob/main/Recursos/2021EAyO_Python_Cls_2.pdf) <br>
- [Video - Clase grabada](https://www.youtube.com/watch?v=d-IBf6-pxf8&feature=youtu.be) <br>

## Clase 3: Trabajando con Cadenas
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-python/-/blob/main/Recursos/2021EAyO_Python_Cls_3.pdf) <br>
- [Video - Clase grabada](https://www.youtube.com/watch?v=LGv6ii_L3H8) <br>

## Clase 4: Trabajando con Funciones
- [Video - Clase grabada](https://www.youtube.com/watch?v=1UZZ-V6D5ME) <br>

## Clase 5: Cerrando con PSeInt
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-python/-/blob/main/Recursos/2021_Python_Cls_5.pdf) <br>

## Clase 6: Python
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-python/-/blob/main/Recursos/2021_Python_Cls_6.pdf) <br>
- [Video - Clase grabada](https://youtu.be/2t8npihvdmY) <br>

## Clase 7: Trabajando con Funciones en Python
- [Video - Clase grabada](https://www.youtube.com/watch?v=cXmno_TA1fA) <br>

## Clase 8: Buscando Palabras
- [Video - Clase grabada](https://www.youtube.com/watch?v=DffM4Rx-8jQ) <br>

## Clase 9: Buscando Palabras (Recargado)
- [Archivo de texto](https://gitlab.com/ciorozco/2021-eayo-python/-/blob/main/Recursos/EAyO_Python_08.txt) <br>
- [Video - Clase grabada](https://www.youtube.com/watch?v=Jc2kuhV64tM) <br>

## Clase 10: TP Final
- [Video - Clase grabada](https://youtu.be/qfI06OVEXKA) <br>

## Clase 11: TP Final (Repaso)
- [Video - Clase grabada](https://youtu.be/35rbLO4S24Q) <br>

